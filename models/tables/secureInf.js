/**********	用户安全信息表			BY  Jaysir 		2015.6.21
***********
***********					可搜索以下关键词来查看未实现功能	
***********	
***********					TODO	: 	未完成
***********					DONE	: 	已完成
***********					TOTEST  : 	待测试
***********					NOTEST  : 	无需测试
***********					WAITING : 	功能待定
***********
***********			接口：
***********						init 		(options , callback)	
***********						addPsw		(options , callback)	
***********						getPswList	(email,callback)	//回调参数列表(err,used_psw数组)
***********							
***********							
***********							
***********/
ar mongoose = require('mongoose');
var Schema = mongoose.Schema;
var secureModel = mongoose.model('secureInf', UserSchema);		//
// 定义用户表结构
var secureSchema = new Schema({
	user_email		:	{type : String , required : true},
	user_reg_ip		:	{type : String , required : true},
	user_used_password:[{type : String }]		//曾用密码
});

// 创建用户时 调用   DONE  TOTEST
secureSecema.statics.init = function (options , callback){
	var inf = {
			user_email	: 	options.email ? options.email : options.user_email,
			user_reg_ip	: 	"",		//  获取Ip   TODO
			"user_used_password.0" :  options.password ? options.password : options.user_password
	}
	var newSecure = new secureModel(inf);
	newSecure.save(callback());
}

// 用户更改密码时调用一次	(保存密码历史记录)    DONE   TOTEST 
secureSchema.statics.addPsw = function(options , callback){
	var email 	=	options.email ? options.email : options.user_email;
	var psw 	=	options.password ? options.password : options.user_password;
	var self = this;
	self.update({user_email:email},{"$push":{user_used_password:psw}},{},callback());
}

// 获取用户曾用密码列表
secureSchema.statics.getPswList = function(email,callback){
	this.findOne({user_email:email},function(err,doc){
		callback(err,doc.user_used_password);
	});
}




module.exports = mongoose.model('secureInf', UserSchema);
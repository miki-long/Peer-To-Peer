/**
 * Created by mcbird on 15/5/13.
 */


/**
 * 左侧导航
 * @type {module|*}
 */
var mainNavigatorControllers = angular.module('mainNavigatorControllers', []);

mainNavigatorControllers.controller('EntryListController', ['$scope', '$routeParams',
	function($scope, $routeParams) {
		var entryName = $routeParams.entryName;
		$scope.EntryList = [];
		var entries = [ENTRY_PROFILE, ENTRY_MOVIES, ENTRY_CHAT, ENTRY_SETTING];
		entries.forEach(function(v, i) {
			var o = {
				name: v,
				active: false
			};
			o.active = (v == entryName) ? true : false;
			$scope.EntryList.push(o);
		})
	}
]);


/**
 * 中间模块入口
 * @type {module|*}
 */
//var entryControllers = angular.module('entryControllers', []);
//
//entryControllers.controller('profileRenderCtrl', ['$scope',
//	function($scope) {
//		$scope.info = 'info from controller';
//		$scope.activityUrl = '#/'+ ENTRY_PROFILE +'/activity';
//	}
//]);
//
//entryControllers.controller('moviesRenderCtrl', ['$scope',
//	function($scope) {
//		$scope.info = 'info from controller';
//	}
//]);
//
//entryControllers.controller('chatRenderCtrl', ['$scope',
//	function($scope) {
//		$scope.info = 'info from controller';
//	}
//]);
//
//entryControllers.controller('settingRenderCtrl', ['$scope',
//	function($scope) {
//		$scope.info = 'info from controller';
//	}
//]);
//
//
//
//var detailControllers = angular.module('detailControllers', []);
//
//detailControllers.controller('activityCtrl', ['$scope',
//	function($scope) {
//		$scope.info = 'info from controller';
//	}
//]);
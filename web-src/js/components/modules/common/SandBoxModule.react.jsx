var React = require('react');

require('../../../../less/public.less');

var tweenState = require('react-tween-state');
var ModuleStore = require('../../../stores/ModuleStore');
var ModuleActions = require('../../../actions/ModuleActions');

/**
 * 一个绝对定位容器，看起来像一个盒子
 * 调用方式： <SandBox width = {700} height = {500}></SandBox>
 * @type {*|Function}
 */
module.exports = React.createClass({
    mixins: [tweenState.Mixin],

    getInitialState: function () {
        var windowWith = window.innerWidth,
            windowHeight = window.innerHeight,
            _width, _height,
            _left, _top;
        _width = windowWith * 0.6 > 700 ? windowWith * 0.6 : 700;
        _height = (windowHeight * 0.6 > 500 && _width > 700) ? windowHeight * 0.6 : 500;
        _left = (windowWith - _width) / 2;
        _top = (windowHeight - _height) / 2;
        return {
            opacity: 0,
            marginLeft: -30,
            width: _width,
            height: _height,
            left: _left,
            top: _top
        }
    },
    handleResize: function(e) {
        var windowWith = window.innerWidth,
            windowHeight = window.innerHeight,
            _width, _height,
            _left, _top;
        _width = windowWith * 0.6 > 700 ? windowWith * 0.6 : 700;
        _height = (windowHeight * 0.6 > 500 && _width > 700) ? windowHeight * 0.6 : 500;
        _left = (windowWith - _width) / 2;
        _top = (windowHeight - _height) / 2;
        this.setState({
            width: _width,
            height: _height,
            left: _left,
            top: _top
        });
    },
    componentDidMount: function(){
        this.showBox();
        window.addEventListener('resize', this.handleResize);
    },
    componentWillUnmount: function() {
        window.removeEventListener('resize', this.handleResize);
    },
    showBox: function(){
        var that = this;
        that.tweenState('opacity', {
            easing: tweenState.easingTypes.easeInOutQuad,
            duration: 500,
            endValue: 1
        });
        that.tweenState('marginLeft', {
            easing: tweenState.easingTypes.easeInOutQuad,
            duration: 500,
            endValue: 0
        });
    },
    render: function(){
        var w = this.props.width ? this.props.width : this.state.width;
        var h = this.props.height ? this.props.height : this.state.height;
        var styleObj = {
            opacity: this.getTweeningValue('opacity'),
            marginLeft: this.getTweeningValue('marginLeft'),
            width: w,
            height: h,
            left: this.state.left,
            top: this.state.top
        };
        return (
            <div className="sandbox" style={styleObj}>
                {this.props.children}
            </div>
        );
    }
});
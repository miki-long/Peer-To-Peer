var React = require('react');

module.exports = React.createClass({
    render: function(){
        var leftNavStyle = {
            left: this.props.leftNavPosLeft
        };
        return (
            <nav className="left-navbar" style={leftNavStyle} onMouseOver={this.props.showLeftNav} onMouseLeave={this.props.hideLeftNav}>
                {this.props.modules}
            </nav>
        );
    }
});